import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  paramId: string;
  goals: any;

  constructor(private route: ActivatedRoute, private router:Router, private _data: DataService) {
    this.route.params.subscribe(res => {
      this.paramId = res.id;
      console.log(res.id)
    })
  }

  ngOnInit() {
    this._data.goal.subscribe(res => this.goals = res);
    this._data.changeGoal(this.goals);
  }

  sendMeBack() {
    this.router.navigate(['']);
}

}
